package domino.spizza;
import java.util.Scanner;
public class DominoSpizza {
    public static void main(String[] args) {
        int code = 0, qty = 0, price = 0, cash = 0;
        String size = "", pizzaType = "", pizzaName = "", pizzaRange = "";
        Scanner sc = new Scanner(System.in);
        System.out.println("=======Domino's Pizza======");
        System.out.println("===========================");
        System.out.println("Enter Pizza Code (1/2/3) : ");
        code = Integer.parseInt(sc.nextLine());
        System.out.println("Enter Pizza Size (L/M/P) : ");
        size = sc.nextLine();
        System.out.println("Enter Pizza Qty          : ");
        qty = Integer.parseInt(sc.nextLine());
        System.out.println("Enter Cash               : ");
        cash = Integer.parseInt(sc.nextLine());
        //get pizza range and name
        switch (code) {
            case 1:
                pizzaRange = "Premium";
                pizzaName = "Italian Supreme";
                break;
            case 2:
                pizzaRange = "Favourite";
                pizzaName = "Double Beef Burger";
                break;
            case 3:
                pizzaRange = "Super Value";
                pizzaName = "Veggie Delight";
                break;
        }
        //get pizza size and price
        switch (size.toUpperCase()) {
            case "L":
                size = "Large";
                if (pizzaRange == "Premium" && pizzaName == "Italian Supreme") {
                    price = 100000;
                }
                if (pizzaRange == "Favourite" && pizzaName == "Double Beef Burger") {
                    price = 90000;
                }
                if (pizzaRange == "Super Value" && pizzaName == "Veggie Delight") {
                    price = 75000;
                }
                break;
            case "M":
                size = "Medium";
                if (pizzaRange == "Premium" && pizzaName == "Italian Supreme") {
                    price = 82000;
                }
                if (pizzaRange == "Favourite" && pizzaName == "Double Beef Burger") {
                    price = 65000;
                }
                if (pizzaRange == "Super Value" && pizzaName == "Veggie Delight") {
                    price = 40000;
                }
                break;
            case "P":
                size = "Personnal";
                if (pizzaRange == "Premium" && pizzaName == "Italian Supreme") {
                    price = 40000;
                }
                if (pizzaRange == "Favourite" && pizzaName == "Double Beef Burger") {
                    price = 32000;
                }
                if (pizzaRange == "Super Value" && pizzaName == "Veggie Delight") {
                    price = 15000;
                }
                break;
        }
        System.out.println("         Domino's Pizza        ");
        System.out.println("");
        System.out.println("===============================");
        System.out.println("");
        System.out.println("Pizza Range    = " + pizzaRange);
        System.out.println("Pizza          = " + pizzaName);
        System.out.println("Pizza Size     = " + size);
        System.out.println("Price          = " + price);
        System.out.println("Qty            = " + qty);
        System.out.println("");
        System.out.println("===============================");
        System.out.println("");
        System.out.println("Total Price    = Rp. " + price);
        System.out.println("Tax & Services = Rp. " + 20000);
        System.out.println("");
        System.out.println("===============================");
        System.out.println("");
        System.out.println("Total Payment  = Rp. " + ((price * qty) + 20000));
        System.out.println("CASH           = Rp. " + cash);
        System.out.println("");
        System.out.println("===============================");
        System.out.println("");
        System.out.println("Ballance Due   = Rp. " + (cash - ((price * qty) + 20000)));
    }
}
